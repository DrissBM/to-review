<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\GitlabService;

class AllProjectsApi
{
    /**
     * @Route(
     *     name="getAllProjects",
     *     path="/api/gitlab_projects",
     *     methods={"GET"}
     * )
     */
    public function __invoke(GitlabService $gitlabService)
    {
        return new JsonResponse(json_decode($gitlabService->getAllProjects()));
    }
}
