<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Security\Core\Security;

class GitlabService
{
    private $client;

    public function __construct(HttpClientInterface $client, Security $security)
    {
        $this->client = $client;
        $this->security = $security;
    }

    public function getAllProjects(): string
    {
        $userGitlabToken = $this->security->getUser()->getAccessToken();

        // Authorization: Bearer OAUTH-TOKEN
        $url = "https://gitlab.com/api/v4/projects?membership=true";
        $response = $this->client->request(
            'GET',
            $url,
            [
                'auth_bearer' => $userGitlabToken
            ]
        );

        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'

        return $content;
    }

    public function getProject(string $id): string
    {
        $userGitlabToken = $this->security->getUser()->getAccessToken();

        $url = "https://gitlab.com/api/v4/projects/" . $id;
        $response = $this->client->request(
            'GET',
            $url,
            [
                'auth_bearer' => $userGitlabToken
            ]
        );
        $url2 = "https://gitlab.com/api/v4/projects/" . $id . "/repository/branches";
        $response2 = $this->client->request(
            'GET',
            $url2,
            [
                'auth_bearer' => $userGitlabToken
            ]
        );

        $content = $response->getContent();
        $content2 = $response2->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'

        return "[" . $content . "," . $content2 . "]";
    }
}
