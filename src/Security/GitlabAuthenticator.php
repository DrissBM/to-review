<?php

namespace App\Security;

use App\Entity\User; // your user entity
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use KnpU\OAuth2ClientBundle\Client\Provider\GitlabClient;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class GitlabAuthenticator extends SocialAuthenticator
{
    private $clientRegistry;
    private $em;
    private $router;

    public function __construct(ClientRegistry $clientRegistry, EntityManagerInterface $em, RouterInterface $router)
    {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
        $this->router = $router;
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse(
            '/connect/gitlab', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }

    public function supports(Request $request)
    {
        // continue ONLY if the current ROUTE matches the check ROUTE
        return $request->attributes->get('_route') === 'connect_check_gitlab';
    }

    public function getCredentials(Request $request)
    {
        // this method is only called if supports() returns true

        // For Symfony lower than 3.4 the supports method need to be called manually here:
        // if (!$this->supports($request)) {
        //     return null;
        // }

        return $this->fetchAccessToken($this->getGitlabClient());
    }

    /**
     * @param AccessToken $credentials
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var GitlabResourceOwner $gitlabUser */
        $gitlabUser = $this->getGitlabClient()
            ->fetchUserFromToken($credentials);

        $email = $gitlabUser->getEmail();

        // 1) have they logged in with Gitlab before? Easy!
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy(['gitlabId' => $gitlabUser->getId()]);
        if (!$user) {
            $user = new User();
            $user->setGitlabId($gitlabUser->getId());
        }

        // // 2) do we have a matching user by email?
        // $userInDatabase = $this->em->getRepository(User::class)
        //     ->findOneBy(['email' => $email]);
        // if ($userInDatabase) {
        //     $userInDatabase->setKeycloakId($gitlabUser->getId());
        //     $this->em->persist($userInDatabase);
        //     $this->em->flush();
        //     return $userInDatabase;
        // }
        // 3) Maybe you just want to "register" them by creating
        // a User object

        $user->setEmail($email);
        $user->setAccessToken($credentials->getToken());
        $user->setRefreshToken($credentials->getRefreshToken());
        $this->em->persist($user);
        $uuid = $user->getId();
        $user->setApiToken($uuid . "-" . md5(rand(PHP_INT_MIN, PHP_INT_MAX)));
        $this->em->flush();
        return $user;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // change "app_homepage" to some route in your app
        return null;

        // or, on success, let the request continue to be handled by the controller
        //return null;
    }

    private function getGitlabClient(): GitlabClient
    {
        return $this->clientRegistry
            // "gitlab" is the key used in config/packages/knpu_oauth2_client.yaml
            ->getClient('gitlab');
    }

    // ...
}
