<?php

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\Routing\Annotation\Route;

class ConnexionApi
{
    private $clientRegistry;
    public function __construct(ClientRegistry $clientRegistry)
    {
        $this->clientRegistry = $clientRegistry;
    }
    /**
     * @Route(
     *     name="register",
     *     path="/api/register",
     *     methods={"GET"}
     * )
     */
    public function __invoke()
    {
        return $this->clientRegistry
            ->getClient('gitlab') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'api', 'profile', 'email' // the scopes you want to access
            ], []);
    }
}
