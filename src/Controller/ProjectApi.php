<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\GitlabService;

class ProjectApi
{
    // public function __construct()
    // {
    // }
    /**
     * @Route(
     *     name="getProject",
     *     path="/api/gitlab_projects/{id}",
     *     methods={"GET"}
     * )
     */
    public function __invoke(GitlabService $gitlabService, $id)
    {
        return new JsonResponse(json_decode($gitlabService->getProject($id)));
    }
}
