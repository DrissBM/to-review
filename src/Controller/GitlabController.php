<?php

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GitlabController extends AbstractController
{
    /**
     * @Route("/gitlab", name="gitlab")
     */
    public function index(Request $request): Response
    {
        dd($request);
        return $this->render('gitlab/index.html.twig', [
            'controller_name' => 'GitlabController'
        ]);
    }


    /**
     * Link to this controller to start the "connect" process
     *
     * @Route("/connect/gitlab", name="connect_start_gitlab")
     */
    public function connect(ClientRegistry $clientRegistry)
    {
        // on Symfony 3.3 or lower, $clientRegistry = $this->get('knpu.oauth2.registry');

        // will redirect to Facebook!
        return $clientRegistry
            ->getClient('gitlab') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'api', 'profile', 'email' // the scopes you want to access
            ], []);
    }

    /**
     * After going to Gitlab, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @Route("/connect/check/gitlab", name="connect_check_gitlab")
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        return $this->json($this->getUser()->getApiToken());
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout(): void
    {
        // controller can be blank: it will never be executed!
    }
}
